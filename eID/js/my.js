 function scaleCaptcha() {
  var reCaptchaWidth = 304;
	var containerWidth = $('.regiter-bd>form').width();
  
  if(reCaptchaWidth > containerWidth) {
    var captchaScale = containerWidth / reCaptchaWidth;
    $('.g-recaptcha').css({
      'transform':'scale('+captchaScale+')'
    });
  }
}
 $(document).on('ready', function() {
$( "#emailInput" ).keyup(function() {
   if ($("#email").hasClass('is-invalid')){
    $("#email label").text('Email khong hop le').addClass("otherclass");
  }else {
    $("#email label").text('Email');
  }
});
$("#eye").click(function() {
	if ($( "#sample4" ).attr('type')=='password'){
		$( "#sample4" ).attr('type','text');
	} else{
		$( "#sample4" ).attr('type','password');
	}
	$("#eye").toggleClass('red-eye');
});
$(".left-menu>li").click(function() {
  $(".left-menu>li").removeClass('menu__active');
  $(this).toggleClass('menu__active');
});
$(".menu-active-button").click(function() {
  var effect = 'slide';
    var options = { direction: 'left' };
    var duration = 500;
    $(".menu-mobile .left-menu").toggle(effect, options, duration);
     $(".dark-screen").toggle(1);
});
scaleCaptcha();
    });


 $(window).resize(
  scaleCaptcha
  );
$(".scroll-to").click(function(){
    var target= $(this).attr('target-scroll');
        offset= $(this).attr('offset'),
        scrollTop=0;
    
    if($(this).parents('ul').css('position')!='fixed'){
      offset=parseInt($(this).attr('offset'))+$(this).parents('ul').height();
    }
    if (target) {
      scrollTop=$(target).offset().top-offset+5
    }
    var scrollTime=Math.abs(scrollTop-$(window).scrollTop())/2;
    $('html,body').animate({
        scrollTop: scrollTop
    }, scrollTime);
});

//fixed menu on scroll

var navigation=$('.header-navigation'),
      fixed_navi='header-navigation__fixed';


$.each(navigation, function (key,value) {
   var navi=$(value);
   var footerOffset=$('.gift--footer__border').offset()?$('.gift--footer__border').offset().top:99999;
   navi.innerWidth(navi.parent().width());
  $(window).resize(function(){
    navi.innerWidth(navi.parent().width());
    }
  );
 
if (navi.offset()){
    var naviOffset=navi.offset().top-navi.innerHeight(),
    naviOffsetTop=navi.offset().top-20,
    removeNavi=footerOffset-navi.outerHeight()-105,
    topTitleUl=$('.top-title ul').offset().top;
    if (navi.hasClass('bbq-free')) {
      naviOffset=navi.parent().offset().top-100;
    }
    $(window).scroll(function(){
       if($(window).scrollTop() >= removeNavi){
          navi.removeClass(fixed_navi);
          if (navi.hasClass('bbq-free')) {
              var topPosition=(removeNavi-parseInt(navi.css('margin-bottom')))+'px';
              navi.css('top',topPosition);
              navi.addClass('position-absolute');
          }
      } else 
      if($(window).scrollTop() >= naviOffset){
        navi.removeClass('position-absolute');
        if (!(navi.hasClass(fixed_navi))) {
          navi.addClass(fixed_navi);
          if (navi.hasClass('bbq-free')) {
            navi.css('top',(naviOffsetTop-topTitleUl)+"px"); 
          } 
        }
      } else {
        if (navi.hasClass(fixed_navi)) {
          navi.removeClass(fixed_navi); 
        }
         if (navi.hasClass('bbq-free')) {
            navi.removeClass('position-absolute');
            navi.css('top',naviOffsetTop+"px");
        }
      }    
    });
}
});
//end fixed menu on scroll


$(document).ready(function(){
if ($('.gift--Will-like')[0]){
  $('.gift--Will-like').slick({
  dots: true,
  infinite: false,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 4,
  autoplay: true,
  autoplaySpeed: 3000,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
        dots: false,
        autoplay: true,
        autoplaySpeed: 1000,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
      }
    }
  ]
});
}
});

$(document).ready(function(){
if ($('.new-detail__footer')[0]){
  $('.new-detail__footer').slick({
  dots: true,
  infinite: false,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 3,
  autoplay: true,
  autoplaySpeed: 3000,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
        dots: false,
        autoplay: true,
        autoplaySpeed: 1000,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
      }
    }
  ]
});
}
});

if ($('.slider-info')[0]){
   $('.slider-info').bxSlider({
    mode: 'fade',
    captions: true
  });
}
if ($('.home-gift--slider1')[0]){
  $('.home-gift--slider1').bxSlider({
  mode: 'fade',
  captions: true
  });
}
if ($('.home-gift--slider2')[0]){
  $('.home-gift--slider2').bxSlider({
  mode: 'fade',
  captions: true
  });
}
if ($('.home-gift--slider3')[0]){
  $('.home-gift--slider3').bxSlider({
  mode: 'fade',
  captions: true
  });
}
